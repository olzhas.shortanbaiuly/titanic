import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

def get_filled():
    df = get_titanic_dataframe()
    
    def extract_title(name):
        if 'Mr.' in name:
            return 'Mr.'
        elif 'Mrs.' in name:
            return 'Mrs.'
        elif 'Miss.' in name:
            return 'Miss.'
        else:
            return 'Other'
    # Extract the title from the name

    df['Title'] = df['Name'].apply(extract_title)
    
    # Calculate median ages for the desired titles
    median_ages = {
        title: int(df[df['Title'] == title]['Age'].median()) for title in ['Mr.', 'Mrs.', 'Miss.']
    }
    
    # Count the number of missing 'Age' values for each title group
    missing_values = {title: df[(df['Title'] == title) & (df['Age'].isnull())].shape[0] for title in ['Mr.', 'Mrs.', 'Miss.']}

    # Fill in missing 'Age' values with the corresponding median age
    for title in ['Mr.', 'Mrs.', 'Miss.']:
        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_ages[title]

    # Prepare the answer as a list of tuples
    answer = (title, missing_values[title], median_ages[title]) for title in ['Mr.', 'Mrs.', 'Miss.']

    return answer
